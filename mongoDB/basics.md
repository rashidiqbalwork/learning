# mongoDB
- **MongoDB is an open-source NoSQL database engine built in C++.**

- **It's a document-store database which means it stores data as a "document" inside a collection, with multiple collections inside a database. Multiple databases can exist for each server.**

## What is schema-less?
- **It means each document can have as many key values as you want with no restriction.**
## SQL and NoSQL
- **SQL is used to communicate with a database.**
- **SQL statements are used to perform tasks such as update data on a database, or retrieve data from a database.**

- **NoSQL is a class of DBMs that are non-relational and generally do not use SQL**
