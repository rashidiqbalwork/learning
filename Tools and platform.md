#  LIST OF TOOLS AND PLATFORM:
## TECHNOLOGY STACKS:-
- IT IS A COMBINATION OF SEVERAL TECHNOLOGY THAT WORKS ALL TOGETHER
- Company uses to build and run an application or project because here you will multiple technology at one place.
**IN TECHNOLOGY STACK THERE ARE MULTIPLE TYPES**
   - **MEAN stack:-** In mean stack there will be **MongoDB**, **ExpressJS**, ** AngularJS**, **NodeJS**
   - **MERN stack:-** MERN stack include these technologies, **MongoDB**, **ExpressJS**, **ReactJS**, **NodeJS**
   - **LAMP stack:-** LAMP stack includes **Linux**, **APache**, **PHP**, **MySQL**
## WHY DO WE USE TECHSTACK
- A tech sack quickly summarizes the programming languages, frameworks, and tools a developer would need to interface with the application.
- Because most coding language have well-known performance attributes and limitations, the tech stack hints at the overall application's strengths and weaknesses.
