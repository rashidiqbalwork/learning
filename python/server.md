## SERVER:
- There are two sides in every website, one is clint side and another one is server side.

- There are many language by which we can make servers but python will be a good option

- In python we have packeges for making the server program.
   - import SimpleHTTPServer  
   - import SocketServer
   - Links:-https://www.tutorialspoint.com/python_network_programming/python_http_server.htm#:~:text=Python%20standard%20library%20comes%20with,is%20accessed%20through%20this%20port.

- **PYTHON** also gives us the **socket programming** in which we can connect server side and clint side.
   - For this you have to import socket and use multiple operating in socket()
   - import socket               # Import socket module
   - s = socket.socket()         # Create a socket object
   - Links:- https://www.tutorialspoint.com/python_network_programming/python_sockets_programming.htm
## MIDDLEWARE:
- **Middlewares is a software that takes output of the one system and processes that data into an format that server will understand by the input in another platform.**
- **Middleware works like a translation service which convert the data into a required format.**
- Middlewares provide a convenient mechanism for filtering HTTP requests entering your application.
- Understand middleware:- https://youtu.be/5uq5wCCXvwU
## API TESTING:
- main job of back end team is to create an API and once api has built then the software testing team will test the api if is it working properly.
- And then the UI team(front end ) will integrate the api together.
- Links for understanding api test:- https://youtu.be/jByWVOcWG9w
### HOW TO DO API TEST:
- There are many tools are avaliable to do an API test.
- **RapidAPI:-** RapidAPI Testing is a cloud-based API testing tool that allows you to create comprehensive API tests (as well as monitor APIs). With RapidAPI testing, you can test all of your REST APIs.
- **Postman** is online api testing platform.
