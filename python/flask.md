# FLASK
- **Flask is a micro web framework written in Python. It is classified as a microframework because it does not require particular tools or libraries.**
- **Flask is used for developing web applications using python**
- **It is designed as a web framework for RESTful API development.**
**IMPORTANT LINKS:** 
- Understand what is **flask**:- https://youtu.be/dC7efRBE-Aw
- simple program using **Flask**:- https://youtu.be/m7UQyy0Q5Uk
