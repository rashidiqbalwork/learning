# PYTHON
## WHAT IS PYTHON?
- Python is a general purpose and poweful programming language.
- It is an interpreted language which runs on python virtual machine, that's why python is quite slower than other compiled.
(interpreterr means it execute the entire code at once even if we found error in previous line)
- It is a High level programming language So, the development of program is much more user friendly.

- Python is comsidered as one of the most versatile programming language as it can be used to develop almost any kind of application including:

  1. Desktip applications
  1. web applications
  1. IoT application
  1. AI,ML and Data science application 

## BRIEF HISTORY OF PYTHON:
- Python laid its foundation in the late 1980s.
- The implementation of Python was started in December 1989 by Guido Van Rossum at CWI in Netherland.
- In February 1991, Guido Van Rossum published the code (labeled version 0.9.0) to alt.sources.
- In 1994, Python 1.0 was released with new features like lambda, map, filter, and reduce.
- Python 2.0 added new features such as list comprehensions, garbage collection systems.
- On December 3, 2008, Python 3.0 (also called "Py3K") was released. It was designed to rectify the fundamental flaw of the language.
- **For details click here** - https://www.javatpoint.com/python-history

## WHY PYTHON IS BEST FOR PROGRAMMERS:

1. **DYNAMICALLY TYPED** 
   - No need to declare variable type
   - Variable can hold different types of value
2. **SUPPORT OF MULTIPLE PROGRAMMING PARADIGMS**
   - Procedural- oriented lang, the program is built around procedures or functions which are nothing but reusable pieces of program
   - Object-oriented , the program us built around object which combine data and functionality.
3. **CROSS PLATFORM**
   - Let's assume we've written a python code for our Windows machine.
   - Now, if we want to run it on a MAC , we don't need to make changes to it for the same.
4. **EXTINSIBLE**
   - Python allows us ti call C/C++/Java code from a python code and thus we say it is an extensible language
   - We generally use this feature when we need a critical piece of code to run very fast.
5. **HUGE LIBRARY**
   - The python Standard Library is hude indeed.
   - it has around 250000 packages for performing many different tasks.
   - Help to do various things like- Voice recognition,Database programming, E-mailing, GUI programming etc
6. **EASY SYNTAX**
   - The syntax of python is quite easy and simple to learn.
## FEW DISADVANTAGES OF PYTHON:
- **SLOW SPEED:-** Python is an interpreted language and dynamically-typed language. The line by line execution of code often leads to slow execution.
- **NOT MEMORY EFFICIENT:-** To provide simplicity to the developer, Python has to do a little tradeoff. The Python programming language uses a large amount of memory. 
- **RUNTIME ERRORS:-** As we know Python is a dynamically typed language so the data type of a variable can change anytime. A variable containing integer number may hold a string in the future, which can lead to Runtime Errors.
## USE CASES OF PYTHON:
- **WEB-DEVELOPMENT**
   - we can use python in backend for bringing security for our website.
   - **With the help of python we can also apply machine learning which will give suggation to users according to their data**
   - Python also support **OOPs** concept so it can helpful for creating great web application
   - The main advantage of developing backend software with Python is its readability.
   - Python underlies these **visual elements** on many sites, driving functionality, **managing databases**, **user accounts**, and much more.
   - Python was used to build a number of the **background services** that make them work.
   - **Data Analytics is a really really important for any websites and python is good for data analytics.**
   - **Frameworks** for web dev in python:- **Django, Flask, Falcon**
- **SOFTWAR DEVELOPMENT**
   - Python is used to develop many different applications and platforms across industries. Notable **examples** include **Instagram** and **Spotify**.
- **DATA SCIENCE**
   - **we can analyse and manipulate millions of data using python library like PANDAS** 
   - **We can also do mathematical operation like statistics part using python lib NumPY**
   - After all that data operation we can predict or decide somethings 
   - Python libraries like Pandas, NumPy, SciPy, and several others help you to work with data and extract valuable information and insights.
   - **Statistic is very important for data analytics**
- **ARTIFICIAL INTELLIGENCE**
   - Probably the most interesting practical uses for Python is in Artificial Intelligence and Machine Learning.
   - Machine Learning algorithms are one of the important real life uses of Python. Developers can write algorithms easily using the programming language.
   - The uses of Python language in AI solutions include advanced computing, data analytics, image recognition,  text & data processing and much more
   - Python has an extensive collection of libraries for Machine Learning applications. These include SciPy, Pandas, Keras, TensorFlow, NumPy and many more.
   - There are so many example of **AI & ML** that we are using in our daily life and in medicals and almost every IT and Non-IT company are working in AI and machine learning to make their business more accurate.
- **GAME DEVELOPMENT** 
   - **NPC(Non playing character) BOTS is a perfect example of ARTIFICIAL INTELLIGENCE and Python**
   - Some of the real world Python projects in the gaming industry include Battlefield 2, Frets on Fire, World of Tanks, etc.
   - Python allows game developers to build tree-based algorithms which are useful in designing different levels in a game. Games require handling multiple requests at once, and Python is extremely fantastic at tha
   - These games use Python libraries like PySoy and PyGame for development.
- **INTERNET oF THINGS(IoT)**
   -  the Internet of Things refers to the rapidly growing network of connected objects that are able to collect and exchange data in real time using embedded sensors.
   1. **How IoT works**
      -  IoT devices contain sensors and mini-computer processors that act on the data collected by the sensors via machine learning.
      - The popular examples of Iot are **GOOGLE SMART HOME  and ALEXA**.

- **AUTOMATION**
   - a process in which a manually performed action is transformed into one that happens automatically.
   - Python is great for automating repetitive tasks, and there are almost endless real world use cases for Python automation.
   - The **smtplib library** is a great resource that will help you **automate your emails**.
- **IMAGE RECOGNATION**
   - python is widely being used in face recognation.
   - **self driving car is also a great example of image recognation and AI**.
- **VOICE RECOGNATION**
   - In python we can easily make voice recognation program with the help of python library **gTTs(google text to speach)**
- **CHAT BOT SYSTEM**
   - Majority of company are using chat bot system to handle their clints or costumer.

## APPLICATION OF PYTHON IN HEALTH CARE:
- **Medical image diagnostic:-** In medical diagnostics, decisions made by doctors are largely based on the results of medical images. These might include Computed Tomography (CT) scans, Magnetic Resonance Imaging (MRI) scans and Of course 
- **PREDICTIVE PROGNOSIS:-** With the similar kind of data we can predict any disease on time.
- **MEDICINE DISCOVERIES:-** Tradiationally they used to do manually matching for which chemicals will be suitable.
   - with the use of Python, bioinformaticians and healthcare research scientists are starting to use computational approaches to replace such manual work 

## IMPORTANCE OF PYTHON IN BUSINESS ANALYTICS:
- **with the help of data analytics we can get the growth of the company and the down part.**
- **And with the help of data analytics and machine learning we can predict what can happen**
- **Expert system also play a major role here for growth of company**
- **With the expert system we can get some advices for the growth.**
- **All these things we can do in python.**

## LIST OF BIG COMPANY THAT USE PYTHON LANGUAGE:
1. **Google** and subsidiaries like **Youtube** use Python for a wide variety of things. In fact, Youtube was built using mostly Python!
1. Industrial Light and **visuals effect**, the company behind the special effects of Star Wars and hundreds of other films, has been using Python for years for its CGI and lighting work.
1. **Facebook** and subsidiaries like **Instagram** use Python for various elements of their infrastructure. Instagram is built entirely using Python and its **Django framework**.
1. **NASA** and associated institutions like the Jet Propulsion Lab use Python for research and scientific purposes.
1. **Netflix** uses Python for server-side data analysis and for a wide variety of back-end apps that help keep the massive streaming service online.
1. **IBM, Intel**, and a variety of other hardware companies use Python for hardware testing.
1. **Quora** is yet another huge social media platform that’s built using lots of Python.

## USE OF PYTHON IN API(application programming interface):
- **MAKING API REQUEST IN PYTHON:-** In order to work with APIs in Python, we need tools that will make those requests. In Python, the most common library for making requests and working with APIs is the requests library. 
   -  The requests library isn’t part of the standard Python library, so you’ll need to install it to get started.
   - use **Pip install requests**
   - There are many different types of requests. The most commonly used one, a GET request, is used to retrieve data. Because we’ll just be working with retrieving data, our focus will be on making ‘get’ requests.
   - To make a ‘GET’ request, we’ll use the requests.get() function, which requires one argument — the URL we want to make the request to.
   - **response = requests.get("https://api.open-notify.org/this-api-doesnt-exist")
   
   print(response.status_code)**

## BEST CODE EDITOR AND IDE FOR PYTHON:
- **PyCharm** - With this you just have to download python interpreter
- **VsCode** -  With this you just have to download python interpreter and some extensions in vs code.
- **Spyder** - Spyder is a scientific integrated development environment written in Python. This software is designed for and by scientists who can integrate with Matplotlib, SciPy, NumPy, Pandas, Cython, IPython, SymPy, and other open-source software. 

## LINTING:
- **Linting highlights syntactical and stylistic problems in your Python source code, which oftentimes helps you identify and correct subtle programming errors or unconventional coding practices that can lead to errors.**
- **Linting detects use of an uninitialized or undefined variable, calls to undefined functions, missing parentheses, and even more subtle issues such as attempting to redefine built-in types or functions.**
- **You can easily enable and disable all linting by using the Python: Enable Linting command.**
- **To enable linters, open the Command Palette (Ctrl+Shift+P) and select the Python: Select Linter command.**
- **For more details about linters go through this:-** https://code.visualstudio.com/docs/python/linting#:~:text=Linting%20highlights%20syntactical%20and%20stylistic,that%20can%20lead%20to%20errors.&text=You%20can%20easily%20enable%20and,the%20Python%3A%20Enable%20Linting%20command.
### WHY DO WE USE LINTER:
- **Linters indentify the bugs and error before the execution the program.**
- **Although there is error indentifier in every compiler but what is your're using some tools that time the use of Linters comes in.**


